# MewinBukkitLib

Collection of functions and code used in my [Bukkit](https://bukkit.org)/[Spigot](https://www.spigotmc.org) projects. This is a library used by other plugins and does not do anything on its own. So do not install it unless you are lead here by another plugin.

## Getting Started

Just clone the repository and compile using maven (`mvn package` to create jar files).

### Prerequisites

You will need to install maven and any JDK > 8.

## Deployment

To install this to a Bukkit/Spigot server simply copy the created jar file (target/MewinBukkitLib-x.x-SNAPSHOT.jar) to the plugins folder.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

You can create issues [on GitLab](https://gitlab.com/mewin/mewinbukkitlib/issues).

## Versioning

For the versions available, see the [tags on this repository](https://gitlab.com/mewin/mewinbukkitlib/tags). 

## Authors

* **Patrick Wuttke** - *Initial work* - [mewin](https://mewin.de)

See also the list of [contributors](https://gitlab.com/mewin/mewinbukkitlib/graphs/master) who participated in this project.

## License

This project is published under the terms of the GNU General Public License v3.0. See the LICENSE file for more information.

## Acknowledgments

* the Bukkit and Spigot devs
* the team of [Avalunia](https://avalunia.de) who provided great ideas and feedback
