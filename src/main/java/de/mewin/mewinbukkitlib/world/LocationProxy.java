/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.world;

import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 *
 * @author mewin
 */
public class LocationProxy
{
    private final String worldName;
    private final double x, y, z;

    public LocationProxy()
    {
        worldName = "";
        x = 0.0;
        y = 0.0;
        z = 0.0;
    }

    public LocationProxy(String world, double x, double y, double z)
    {
        this.worldName = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public LocationProxy(Location location)
    {
        this(location.getWorld().getUID().toString(), location.getX(), location.getY(), location.getZ());
    }

    public Location getLocation()
    {
        World world;
        try {
            world = Bukkit.getWorld(UUID.fromString(worldName));
        } catch (IllegalArgumentException ex) {
            world = Bukkit.getWorld(worldName);
        }
        if (world == null) {
            world = Bukkit.getWorlds().get(0);
        }

        return new Location(world, x, y, z);
    }

    public String getWorldName()
    {
        return worldName;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public double getZ()
    {
        return z;
    }
}
