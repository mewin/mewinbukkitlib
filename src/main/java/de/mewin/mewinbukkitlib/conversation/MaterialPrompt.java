/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public abstract class MaterialPrompt extends StringPrompt
{
    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        CommandSender receiver = (CommandSender) context.getForWhom();
        Material mat = Material.matchMaterial(input);

        if (mat != null) {
            return acceptInput(context, mat);
        }

        CHAT_HELPER.sendMessage(receiver, "material-invalid");
        return this;
    }

    protected abstract Prompt acceptInput(ConversationContext context, Material input);
}
