/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

/**
 *
 * @author mewin
 */
public interface EnumAcceptor<T extends Enum>
{
    public T getEnum();
    public boolean setEnum(T value);
}
