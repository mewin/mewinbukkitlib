/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class EnumAcceptorPrompt<T extends Enum> extends StringPrompt
{
    private final Prompt parentPrompt;
    private final String promptid;
    private final EnumAcceptor<T> acceptor;
    private final Class<T> clazz;

    public EnumAcceptorPrompt(Prompt parentPrompt, String promptid, EnumAcceptor<T> acceptor, Class<T> clazz)
    {
        this.parentPrompt = parentPrompt;
        this.promptid = promptid;
        this.acceptor = acceptor;
        this.clazz = clazz;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), promptid);
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        T value;
        try {
            value = (T) Enum.valueOf(clazz, input.trim().toUpperCase());
        } catch(IllegalArgumentException ex) {
            return this;
        }

        if (!acceptor.setEnum(value)) {
            return this;
        }

        if (parentPrompt instanceof MenuPrompt) {
            ((MenuPrompt) parentPrompt).reloadMenu();
        }

        return parentPrompt;
    }
}
