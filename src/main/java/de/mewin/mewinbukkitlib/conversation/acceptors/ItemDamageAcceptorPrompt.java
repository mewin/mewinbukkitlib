/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.NumericPrompt;
import org.bukkit.conversations.Prompt;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemDamageAcceptorPrompt extends NumericPrompt
{
    private final ItemStackAcceptor requirement;
    private final MenuPrompt parentPrompt;

    public ItemDamageAcceptorPrompt(ItemStackAcceptor requirement, MenuPrompt parentPrompt)
    {
        this.requirement = requirement;
        this.parentPrompt = parentPrompt;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), "prompt-item-damage");
    }

    @Override
    protected boolean isNumberValid(ConversationContext context, Number input)
    {

        int val = input.intValue();

        return val >= 0 && val <= Short.MAX_VALUE;
    }

    @Override
    protected Prompt acceptValidatedInput(ConversationContext context, Number input)
    {
        ItemStack items = requirement.getItemStack();

        items.setDurability(input.shortValue());

        parentPrompt.reloadMenu();
        return parentPrompt;
    }
}
