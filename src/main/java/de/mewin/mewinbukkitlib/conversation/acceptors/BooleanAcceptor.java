/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

/**
 *
 * @author mewin
 */
public interface BooleanAcceptor
{
    public boolean setBoolean(boolean value);
    public boolean getBoolean();
}
