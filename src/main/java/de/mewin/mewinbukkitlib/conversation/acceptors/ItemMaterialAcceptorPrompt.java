/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import de.mewin.mewinbukkitlib.conversation.MaterialPrompt;
import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.Material;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.inventory.ItemStack;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class ItemMaterialAcceptorPrompt extends MaterialPrompt
{

    private final ItemStackAcceptor acceptor;
    private final MenuPrompt parentPrompt;

    public ItemMaterialAcceptorPrompt(ItemStackAcceptor acceptor, MenuPrompt parentPrompt)
    {
        this.acceptor = acceptor;
        this.parentPrompt = parentPrompt;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), "prompt-item-material");
    }

    @Override
    protected Prompt acceptInput(ConversationContext context, Material input)
    {
        ItemStack items = acceptor.getItemStack();
        if (items == null) {
            acceptor.setItemStack(new ItemStack(input));
        }
        else {
            items.setType(input);
        }

        parentPrompt.reloadMenu();
        return parentPrompt;
    }
}
