/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

/**
 *
 * @author mewin
 */
public interface IntAcceptor
{
    public boolean setInt(int value);
    public int getInt();
}
