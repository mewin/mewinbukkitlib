/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

/**
 *
 * @author mewin
 */
public interface DoubleAcceptor
{
    public boolean setDouble(double value);
    public double getDouble();
}
