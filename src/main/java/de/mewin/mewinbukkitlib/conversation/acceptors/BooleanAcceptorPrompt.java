/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.conversations.BooleanPrompt;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class BooleanAcceptorPrompt extends BooleanPrompt
{
    private final Prompt parentPrompt;
    private final String promptid;
    private final BooleanAcceptor acceptor;

    public BooleanAcceptorPrompt(Prompt parentPrompt, String promptid, BooleanAcceptor acceptor)
    {
        this.parentPrompt = parentPrompt;
        this.promptid = promptid;
        this.acceptor = acceptor;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), promptid);
    }

    @Override
    protected Prompt acceptValidatedInput(ConversationContext context, boolean input)
    {
        if (!acceptor.setBoolean(input)) {
            return this;
        }
        
        if (parentPrompt instanceof MenuPrompt) {
            ((MenuPrompt) parentPrompt).reloadMenu();
        }

        return parentPrompt;
    }
}
