/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import org.bukkit.conversations.Prompt;
import org.bukkit.entity.EntityType;

/**
 *
 * @author mewin
 */
public class EntityTypeAcceptorPrompt extends EnumAcceptorPrompt<EntityType>
{
    public EntityTypeAcceptorPrompt(Prompt parentPrompt, String promptid, EnumAcceptor<EntityType> acceptor, Class<EntityType> clazz)
    {
        super(parentPrompt, promptid, acceptor, clazz);
    }
}
