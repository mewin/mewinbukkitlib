/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import org.bukkit.Location;

/**
 *
 * @author mewin
 */
public interface LocationAcceptor
{
    public boolean setLocation(Location location);
    public Location getLocation();
}
