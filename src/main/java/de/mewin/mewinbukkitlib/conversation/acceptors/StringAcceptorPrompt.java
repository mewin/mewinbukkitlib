/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class StringAcceptorPrompt extends StringPrompt
{
    private final Prompt parentPrompt;
    private final String promptid;
    private final StringAcceptor acceptor;

    public StringAcceptorPrompt(Prompt parentPrompt, String promptid, StringAcceptor acceptor)
    {
        this.promptid = promptid;
        this.acceptor = acceptor;
        this.parentPrompt = parentPrompt;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        return CHAT_HELPER.getMessage(context.getForWhom(), promptid);
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        if (!acceptor.setString(input)) {
            return this;
        }

        if (parentPrompt instanceof MenuPrompt) {
            ((MenuPrompt) parentPrompt).reloadMenu();
        }

        return parentPrompt;
    }
}
