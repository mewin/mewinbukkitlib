/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import org.bukkit.inventory.ItemStack;

/**
 *
 * @author mewin
 */
public interface ItemStackAcceptor
{
    public void setItemStack(ItemStack itemStack);
    public ItemStack getItemStack();
}
