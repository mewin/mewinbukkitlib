/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.acceptors;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;
import org.bukkit.entity.Player;

import java.util.UUID;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public class LocationAcceptorPrompt extends StringPrompt
{
    private final Prompt parentPrompt;
    private final LocationAcceptor acceptor;
    private final String promptid;

    public LocationAcceptorPrompt(Prompt parentPrompt, String promptid, LocationAcceptor acceptor)
    {
        this.parentPrompt = parentPrompt;
        this.promptid = promptid;
        this.acceptor = acceptor;
    }

    private Location parseLocation(ConversationContext context, String input)
    {
        Conversable conv = context.getForWhom();
        String locin = input;
        World locworld = acceptor.getLocation().getWorld();

        if (input.contains(":"))
        {
            String[] parts = input.split(":");

            if (parts.length != 2) {
                return null;
            }

            String worldName = parts[0].trim();

            // try world per uuid
            try {
                locworld = Bukkit.getWorld(UUID.fromString(worldName));
            } catch(IllegalArgumentException ex) {
                locworld = Bukkit.getWorld(worldName);
            }
            if (locworld == null) {
                return null;
            }
            locin = parts[1].trim();
        }
        else if (conv instanceof Player)
        {
            locworld = ((Player) conv).getWorld();
        }

        String[] parts = locin.split(" +");
        if (parts.length != 3) {
            return null;
        }

        double[] nums = new double[3];
        for (int i = 0; i < 3; ++i)
        {
            try {
                nums[i] = Double.valueOf(parts[i]);
            } catch(NumberFormatException ex) {
                return null;
            }
        }

        return new Location(locworld, nums[0], nums[1], nums[2]);
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        Location loc;

        if (input.trim().equalsIgnoreCase("here"))
        {
            Conversable conv = context.getForWhom();
            if (!(conv instanceof Player))
            {
                CHAT_HELPER.sendMessage((CommandSender) conv, "input-player-only");
                return this;
            }
            loc = ((Player) conv).getLocation();
        }
        else {
            loc = parseLocation(context, input);
        }

        if (loc == null) {
            return this;
        }

        if (!acceptor.setLocation(loc)) {
            return this;
        }


        if (parentPrompt instanceof MenuPrompt) {
            ((MenuPrompt) parentPrompt).reloadMenu();
        }

        context.setSessionData(LocationAcceptorPrompt.IN_LOCATION_PROMPT, false);

        return parentPrompt;
    }

    @Override
    public String getPromptText(ConversationContext context)
    {
        context.setSessionData(LocationAcceptorPrompt.IN_LOCATION_PROMPT, true);
        return CHAT_HELPER.getMessage(context.getForWhom(), promptid);
    }

    public static final String IN_LOCATION_PROMPT = "in-location-prompt";
}
