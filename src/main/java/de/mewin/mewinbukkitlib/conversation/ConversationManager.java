/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation;

import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationAbandonedEvent;
import org.bukkit.conversations.ConversationAbandonedListener;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Utility class to manage conversations.
 *
 * Used to track running conversations of the plugin.
 *
 * @author mewin
 */
public class ConversationManager
{
    private final HashMap<UUID, Conversation> conversations;

    public ConversationManager()
    {
        this.conversations = new HashMap<>();
    }

    public void beginConversation(Conversation conversation)
    {
        final Player player = (Player) conversation.getForWhom();

        abandonConversation(player);
        conversation.addConversationAbandonedListener(new ConversationAbandonedListener()
        {
            @Override
            public void conversationAbandoned(ConversationAbandonedEvent abandonedEvent)
            {
                conversations.remove(player.getUniqueId());
            }
        });
        conversations.put(player.getUniqueId(), conversation);
        conversation.begin();
    }

    public void abandonConversation(Player player)
    {
        Conversation conversation = getCurrentConversation(player);
        if (conversation != null) {
            conversation.abandon();
        }
    }

    public Conversation getCurrentConversation(Player player)
    {
        return conversations.get(player.getUniqueId());
    }
}
