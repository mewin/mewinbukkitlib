/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation.editors;

import de.mewin.mewinbukkitlib.conversation.MenuPrompt;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.Prompt;

import java.util.List;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public abstract class EditListPrompt<T> extends MenuPrompt
{
    protected final Prompt parentPrompt;

    public EditListPrompt(Prompt parentPrompt)
    {
        super();

        this.parentPrompt = parentPrompt;
    }

    @Override
    protected void fillOptions(CommandSender sender)
    {
        int index = 0;
        for (T object : getObjects())
        {
            addOptionFor(index, object, sender);
            ++index;
        }

        addComplexOption(CHAT_HELPER.getMessage(sender, getMsgIdNew()), getAddPrompt(sender));
        addSimpleOption(CHAT_HELPER.getMessage(sender, "menu-done"), parentPrompt);
    }

    protected abstract void addOptionFor(int index, T object, CommandSender sender);
    protected abstract String getMsgIdNew();
    protected abstract List<T> getObjects();
    protected abstract PromptSupplier getAddPrompt(CommandSender sender);
}
