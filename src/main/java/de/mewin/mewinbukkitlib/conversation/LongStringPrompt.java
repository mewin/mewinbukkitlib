/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

/**
 *
 * @author mewin
 */
public abstract class LongStringPrompt extends StringPrompt
{
    protected final String endSequence;
    private StringBuilder stringBuilder;

    public LongStringPrompt(String endSequence)
    {
        this.endSequence = endSequence;
        this.stringBuilder = new StringBuilder();
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input)
    {
        if (input.trim().equals(endSequence)) {
            String res = stringBuilder.toString().trim();
            stringBuilder.setLength(0);
            return acceptFinalInput(context, res);
        }

        stringBuilder.append(input).append("\n");

        return this;
    }

    protected abstract Prompt acceptFinalInput(ConversationContext context, String input);
}
