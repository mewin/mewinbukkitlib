/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.conversation;

import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.RunCommandEvent;
import de.mewin.mewinbukkitlib.json.ShowTextEvent;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.NumericPrompt;
import org.bukkit.conversations.Prompt;

import java.util.ArrayList;
import java.util.Collection;

import static de.mewin.mewinbukkitlib.MewinBukkitLibPlugin.CHAT_HELPER;

/**
 *
 * @author mewin
 */
public abstract class MenuPrompt extends NumericPrompt
{
    protected final ArrayList<MenuOption> options;

    public MenuPrompt(Collection<MenuOption> options)
    {
        this.options = new ArrayList<>(options);
    }

    public MenuPrompt()
    {
        this(new ArrayList<MenuOption>());
    }

    public void reloadMenu()
    {
        options.clear();
    }

    protected void addOption(MenuOption option)
    {
        options.add(option);
    }

    protected void addSimpleOption(String label, Prompt prompt)
    {
        addOption(new SimpleMenuOption(label, prompt));
    }

    protected void addUpdatingOption(String label, final MenuPrompt prompt)
    {
        addOption(new MenuOption(label)
        {
            @Override
            public Prompt handleSelect()
            {
                prompt.reloadMenu();

                return prompt;
            }
        });
    }

    protected void addComplexOption(String label, final PromptSupplier supplier)
    {
        MenuOption option = new MenuOption(label)
        {
            @Override
            public Prompt handleSelect()
            {
                return supplier.get();
            }
        };

        addOption(option);
    }

    protected void printOptions(CommandSender receiver)
    {
        if (options.isEmpty()) {
            fillOptions(receiver);
        }

        for (int i = 0; i < options.size(); ++i)
        {
            JSONText[] texts = new JSONText.MultiBuilder()
                    .setText(CHAT_HELPER.getMessage(receiver, "menu-entry", i + 1, options.get(i).getLabel()))
                    .setHoverEvent(new ShowTextEvent(CHAT_HELPER.getMessage(receiver, "menu-entry-hover")))
                    .setClickEvent(new RunCommandEvent("/bq_callback conversation-send " + (i + 1))) // TODO: shouldnt use bq_callback
                    .build();
            CHAT_HELPER.trySendJSONMessage(receiver, texts);
            // CHAT_HELPER.sendMessage(receiver, "menu-entry", i + 1, options.get(i).getLabel());
        }
    }

    @Override
    protected boolean isNumberValid(ConversationContext context, Number input)
    {
        int index = input.intValue();
        return index > 0 && index <= options.size();
    }

    @Override
    protected Prompt acceptValidatedInput(ConversationContext context, Number input)
    {
        int index = input.intValue() - 1;

        Prompt result = options.get(index).handleSelect();
        
        return result;
    }

    protected abstract void fillOptions(CommandSender receiver);

    public static abstract class MenuOption
    {
        private final String label;

        public MenuOption(String label)
        {
            this.label = label;
        }

        public String getLabel()
        {
            return label;
        }

        public abstract Prompt handleSelect();
    }

    public static class SimpleMenuOption extends MenuOption
    {
        private final Prompt prompt;

        public SimpleMenuOption(String label, Prompt prompt)
        {
            super(label);
            this.prompt = prompt;
        }

        @Override
        public Prompt handleSelect()
        {
            return prompt;
        }
    }

    public static abstract class PromptSupplier
    {
        public abstract Prompt get();
    }
}
