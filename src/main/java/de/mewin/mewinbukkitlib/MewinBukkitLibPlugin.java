/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib;

import de.mewin.mewinbukkitlib.addons.Addon;
import de.mewin.mewinbukkitlib.addons.ProtocolLibAddon;
import de.mewin.mewinbukkitlib.chat.ChatHelper;
import de.mewin.mewinbukkitlib.chat.DefaultMessageBuilder;
import de.mewin.mewinbukkitlib.conversation.ConversationManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

/**
 *
 * @author mewin
 */
public class MewinBukkitLibPlugin extends MewinPluginBase
{
    @Override
    public void onLoad()
    {
        CHAT_HELPER.setMessageBuilder(new DefaultMessageBuilder(this));
    }

    @Override
    public void onEnable()
    {
        CHAT_HELPER.loadMessages(this);

        loadAddons();
    }

    @Override
    public void onDisable()
    {
        disableAddons();
    }

    private void loadAddons()
    {
        PluginManager pm = Bukkit.getPluginManager();

        if (pm.getPlugin("ProtocolLib") != null) {
            addons.add(new ProtocolLibAddon(this));
        }

        enableAddons();
    }

    public static MewinBukkitLibPlugin get()
    {
        return getPlugin(MewinBukkitLibPlugin.class);
    }

    public static final ChatHelper CHAT_HELPER = new ChatHelper();
    public static final ConversationManager CONVERSATION_MANAGER = new ConversationManager();
}
