package de.mewin.mewinbukkitlib.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class LocaleChangedEvent extends Event
{
    private final Player player;
    private final String language;

    public LocaleChangedEvent(Player player, String language)
    {
        this.player = player;
        this.language = language;
    }

    public Player getPlayer()
    {
        return player;
    }

    public String getLanguage()
    {
        return language;
    }

    @Override
    public HandlerList getHandlers()
    {
        return getHandlerList();
    }

    public static HandlerList getHandlerList()
    {
        return HANDLER_LIST;
    }

    private static final HandlerList HANDLER_LIST = new HandlerList();
}
