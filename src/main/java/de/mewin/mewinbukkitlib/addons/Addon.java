/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.addons;

/**
 * An addon to the plugin.
 *
 * Used internally to bundle functionality that can be en- and disabled together.
 * Addons are saved inside the plugin class and are en-/disabled together with
 * the actual plugin.
 *
 * @author mewin
 */
public abstract class Addon
{
    /**
     * Called when the plugin is enabled.
     */
    public void onEnable() {}
    /**
     * Called when the plugin is disabled.
     */
    public void onDisable() {}
}
