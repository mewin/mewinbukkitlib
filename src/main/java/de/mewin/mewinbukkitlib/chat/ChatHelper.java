/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.chat;

import de.mewin.mewinbukkitlib.json.JSONText;
import de.mewin.mewinbukkitlib.json.RunCommandEvent;
import de.mewin.mewinbukkitlib.json.ShowItemRawEvent;
import de.mewin.mewinbukkitlib.json.ShowTextEvent;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.conversations.Conversable;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.json.simple.JSONArray;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author mewin
 */
public class ChatHelper
{
    private MessageBuilder messageBuilder;
    private boolean havePlaceholdersAPI = false;

    public void setMessageBuilder(MessageBuilder messageBuilder)
    {
        if (this.messageBuilder == null) {
            this.messageBuilder = messageBuilder;
        }
    }

    public void setHavePlaceholdersAPI(boolean havePlaceholdersAPI)
    {
        this.havePlaceholdersAPI = havePlaceholdersAPI;
    }

    public void loadMessages(Plugin plugin)
    {
        this.messageBuilder.loadMessages(plugin);
    }

    public String getMessage(Object receiver, String msgId, Object ... args)
    {
        return messageBuilder.getMessage(receiver, msgId, args);
    }

    public String tryGetMessage(Object receiver, String msgId, Object ... args)
    {
        if (!messageBuilder.containsMessage(msgId)) {
            return null;
        }
        return messageBuilder.getMessage(receiver, msgId, args);
    }

    public void sendMessage(CommandSender receiver, String msgId, Object ... args)
    {
        String message = getMessage(receiver, msgId, args);

        if (receiver instanceof Conversable)
        {
            Conversable conversable = (Conversable) receiver;

            if (conversable.isConversing()) {
                conversable.sendRawMessage(message);
                return;
            }
        }

        receiver.sendMessage(message);
    }

    public void trySendMessage(Object receiver, String msgId, Object ... args)
    {
        if (receiver instanceof CommandSender) {
            sendMessage((CommandSender) receiver, msgId, args);
        }
    }

    public void sendJSONMessage(Player receiver, JSONText[] texts)
    {
        JSONArray array = new JSONArray();
        for (JSONText text : texts) {
            array.add(text.toJSON());
        }

        if (array.isEmpty()) { // minecraft crashes when receiving '[]' as json
            return;
        }

        Server server = Bukkit.getServer();
        ConsoleCommandSender consoleSender = Bukkit.getConsoleSender();
        server.dispatchCommand(consoleSender, "tellraw " + receiver.getUniqueId().toString()
                                                + " " + array.toJSONString());
    }

    public void trySendJSONMessage(Object receiver, JSONText[] texts)
    {
        if (receiver instanceof Player) {
            sendJSONMessage((Player) receiver, texts);
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            for (JSONText text : texts) {
                sb.append(text.getText());
            }
            trySendMessage(receiver, sb.toString());
       }
    }

    public void sendUserMessage(Player receiver, String userString)
    {
        sendJSONMessage(receiver, formatUserMessage(receiver, userString));
    }

    public void sendUserMessage(OfflinePlayer offlinePlayer, String userString)
    {
        Player player = offlinePlayer.getPlayer();

        if (player != null) {
            sendUserMessage(player, userString);
        }
    }

    private void formatHoverTag(Player player, String params, JSONText.MultiBuilder builder)
    {
        builder.setHoverEvent(new ShowTextEvent(params));
    }

    private void formatCommandTag(Player player, String params, JSONText.MultiBuilder builder)
    {
        builder.setClickEvent(new RunCommandEvent(params));
    }

    private void formatItemTag(Player player, String params, JSONText.MultiBuilder builder)
    {
        builder.setHoverEvent(new ShowItemRawEvent("{" + params + "}"));
    }

    private void formatUserTag(Player player, String tag, JSONText.MultiBuilder builder)
    {
        String cmd = tag;
        String params = "";

        int colonPos = cmd.indexOf(':');
        if (colonPos > -1) {
            params = cmd.substring(colonPos + 1);
            cmd = cmd.substring(0, colonPos);
        }

        switch (cmd)
        {
            case "player":
                builder.setText(player.getDisplayName());
                break;
            case "hover":
                formatHoverTag(player, params, builder);
                return;
            case "command":
                formatCommandTag(player, params, builder);
                return;
            case "item":
                formatItemTag(player, params, builder);
                return;
//            case "test":
//                builder.setHoverEvent(new ShowItemEvent(new ItemStack(Material.STONE)));
//                return;
            default:
                return;
        }
        builder.next();
    }

    public JSONText[] formatUserMessage(Player player, String userString)
    {
        Matcher matcher = USER_TAG_PATTERN.matcher(userString);
        JSONText.MultiBuilder builder = new JSONText.MultiBuilder();

        int last = 0;

        while (matcher.find())
        {
            int matchFirst = matcher.start();
            int matchLast = matcher.end();

            if (matchFirst > last)
            { // text in between
                String msg = userString.substring(last, matchFirst);
                builder.setText(ChatColor.translateAlternateColorCodes('&', msg)).next();
            }

            formatUserTag(player, matcher.group(1), builder);

            last = matchLast;
        }

        if (last < userString.length() - 1)
        {
            String msg = userString.substring(last);
            String result;
            if (havePlaceholdersAPI) {
                result = PlaceholderAPI.setPlaceholders(player, msg);
            } else {
                result = ChatColor.translateAlternateColorCodes('&', msg);
            }
            builder.setText(result);
        }

        return builder.build();
    }

    public String formatPlayerString(String msg, Player player) // TODO: support more strings/placeholders api
    {
        return ChatColor.translateAlternateColorCodes('&', msg.replace("{login}", player.getName())
                .replace("{player}", player.getDisplayName()));
    }

    public String formatMessage(Player player, String message, Object ... args)
    {
        return formatPlayerString(getMessage(player, message, args), player);
    }

    private static final Pattern USER_TAG_PATTERN = Pattern.compile("\\{([^\\}]+)}");
}
