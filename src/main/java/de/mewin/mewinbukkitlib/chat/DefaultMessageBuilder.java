/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.chat;

import de.mewin.mewinbukkitlib.constants.MetaKeys;
import de.mewin.mewinbukkitlib.util.PlayerUtil;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.text.FieldPosition;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 *
 * @author mewin
 */
public class DefaultMessageBuilder extends MessageBuilder
{
    private final HashMap<String, MessageFormat> messageMap;
    private final HashMap<String, HashMap<String, MessageFormat>> localizedMessageMaps;
    private final Plugin plugin;
    private String defaultLocale;

    public DefaultMessageBuilder(Plugin plugin)
    {
        this.plugin = plugin;
        this.defaultLocale = "en_US";
        this.messageMap = new HashMap<>();
        this.localizedMessageMaps = new HashMap<>();
    }

    public String getDefaultLocale()
    {
        return defaultLocale;
    }

    public void setDefaultLocale(String defaultLocale)
    {
        this.defaultLocale = defaultLocale;
    }

    private void loadMessages(InputStream in, HashMap<String, MessageFormat> targetMap)
    {
        Yaml yaml = new Yaml();

        Map<String, Object> rawMessages = (Map<String, Object>) yaml.load(in);

        for (Map.Entry<String, Object> message : rawMessages.entrySet())
        {
            String key = message.getKey();
            String format = (String) message.getValue();
            targetMap.put(key, new MessageFormat(format));
        }
    }

    private void loadMessages(Plugin plugin, String filename, HashMap<String, MessageFormat> targetMap)
    {
        InputStream in = plugin.getClass().getResourceAsStream("/" + filename);

        if (in != null)
        {
            plugin.getLogger().log(Level.INFO, "Loading messages from {0}", filename);
            loadMessages(in, targetMap);
        }

        File dataFolder = plugin.getDataFolder();
        File langFile = new File(dataFolder, filename);
        if (langFile.exists())
        {
            plugin.getLogger().log(Level.INFO, "Loading additional messages from {0}/{1}",
                                   new Object[] { dataFolder.getName(), filename });

            try (FileInputStream fin = new FileInputStream(langFile)) {
                loadMessages(fin, targetMap);
            }
            catch(IOException ex) {
                plugin.getLogger().log(Level.WARNING, "Could not load messages:", ex);
            }
        }
    }

    private void loadI18N(Plugin plugin, InputStream i18nStream)
    {
        Yaml yaml = new Yaml();

        Object data = yaml.load(i18nStream);
        if ((data instanceof Map))
        {
            for (Map.Entry entry : ((Map<?, ?>) data).entrySet())
            {
                String lang = entry.getKey().toString().toLowerCase();
                String filename = entry.getValue().toString();

                if (!localizedMessageMaps.containsKey(lang)) {
                    localizedMessageMaps.put(lang, new HashMap<String, MessageFormat>());
                }

                loadMessages(plugin, filename, localizedMessageMaps.get(lang));
            }
        }
    }

    private void loadI18N(Plugin plugin, String filename)
    {
        InputStream in = plugin.getClass().getResourceAsStream("/" + filename);

        if (in != null)
        {
            plugin.getLogger().log(Level.INFO, "Loading i18n data from {0}", filename);
            loadI18N(plugin, in);
        }

        File dataFolder = plugin.getDataFolder();
        File langFile = new File(dataFolder, filename);
        if (langFile.exists())
        {
            plugin.getLogger().log(Level.INFO, "Loading additional i18n data from {0}/{1}",
                                   new Object[] { dataFolder.getName(), filename });

            try (FileInputStream fin = new FileInputStream(langFile)) {
                loadI18N(plugin, fin);
            }
            catch(IOException ex) {
                plugin.getLogger().log(Level.WARNING, "Could not load messages:", ex);
            }
        }
    }

    @Override
    public void loadMessages(Plugin plugin)
    {
        loadMessages(plugin, "strings.yml", messageMap);

        loadI18N(plugin, "i18n.yml");
    }

    private String getLocale(Object receiver)
    {
        if (!(receiver instanceof Player)) {
            return null;
        }

        MetadataValue value = PlayerUtil.getPlayerMeta(plugin, (Player) receiver, MetaKeys.LOCALE);

        if (value != null) {
            return value.asString().toLowerCase();
        }

        return defaultLocale;
    }

    @Override
    public boolean containsMessage(String msgId)
    {
        return messageMap.containsKey(msgId);
    }

    @Override
    public String getMessage(Object receiver, String msgId, Object... args)
    {
        HashMap<String, MessageFormat> recMap = null;
        String locale = getLocale(receiver);

        if (locale != null) {
            recMap = localizedMessageMaps.get(locale);
        }

        if (recMap == null) {
            recMap = messageMap;
        }

        MessageFormat format = recMap.get(msgId);

        if (format == null) {
            format = messageMap.get(msgId);
        }

        if (format == null)
        {
            plugin.getLogger().log(Level.WARNING, "Missing translation for key: {0}", msgId);
            return ChatColor.RED + "!!" + msgId;
        }

        String message = format.format(args, new StringBuffer(), new FieldPosition(0)).toString();
        return ChatColor.translateAlternateColorCodes('&', message);
    }

    private static final FilenameFilter YAML_FILTER = new FilenameFilter()
    {
        @Override
        public boolean accept(File file, String name)
        {
            return name.toLowerCase().endsWith(".yml");
        }
    };
}
