package de.mewin.mewinbukkitlib;

import de.mewin.mewinbukkitlib.addons.Addon;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.logging.Level;

public class MewinPluginBase extends JavaPlugin
{
    protected final ArrayList<Addon> addons = new ArrayList<>();

    protected void loadConfig()
    {
        getConfig().options().copyDefaults(true);
        // load default values
        InputStream defIn = getClass().getResourceAsStream("/default_config.yml");
        if (defIn != null)
        {
            try
            {
                YamlConfiguration defConf = new YamlConfiguration();
                defConf.load(defIn);
                getConfig().setDefaults(defConf.getRoot());
            }
            catch(IOException | InvalidConfigurationException ex) {
                getLogger().log(Level.WARNING, "could not load default configuration values");
            }
        }
        File configFile = new File(getDataFolder(), "config.yml");
        if (configFile.exists())
        {
            try
            {
                getConfig().load(configFile);
            }
            catch(IOException | InvalidConfigurationException ex)
            {
                getLogger().log(Level.WARNING, "Could not load configuration.", ex);
            }
        }
        else
        {
            InputStream in = getClass().getResourceAsStream("/config.yml");
            if (in != null) // just in case
            {
                try
                {
                    getLogger().log(Level.INFO, "No configuration file found, creating a default one.");
                    Files.copy(in, configFile.toPath());
                }
                catch (IOException ex)
                {
                    getLogger().log(Level.SEVERE, "Could not create default configuration.", ex);
                }
            }
        }
    }

    protected final void enableAddons()
    {
        addons.forEach(Addon::onEnable);
    }

    protected final void disableAddons()
    {
        addons.forEach(Addon::onDisable);
    }
}
