/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.util;

import com.comphenix.protocol.utility.MinecraftReflection;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/**
 *
 * @author mewin
 */
public class Items
{
    public static boolean compareStacks(ItemStack stack1, ItemStack stack2, boolean ignoreDurability)
    {
        if (stack1 == stack2) {
            return true;
        }

        if (stack1 == null || stack2 == null) {
            return false;
        }

        if (!ignoreDurability) {
            return stack1.isSimilar(stack2);
        }

        ItemStack stack1a = stack1.clone();
        ItemStack stack2a = stack2.clone();

        stack1a.setDurability((short) 0);
        stack2a.setDurability((short) 0);

        return stack1a.isSimilar(stack2a);
    }

    public static Object getMinecraftStack(ItemStack stack)
    {
        return MinecraftReflection.getMinecraftItemStack(stack);
//        if (stack.getType() == Material.AIR) {
//            return null;
//        }
//
//        try
//        {
//            Class<?> cls = stack.getClass(); // should be org.bukkit.craftbukkit.inventory.CraftItemStack
//            Method methodAsNMSCopy = cls.getMethod("asNMSCopy", ItemStack.class);
//
//            return methodAsNMSCopy.invoke(null, stack);
//        }
//        catch(IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException ex)
//        {
//            Bukkit.getLogger().log(Level.WARNING, "", ex);
//            return null;
//        }
    }

    public static Object getMinecraftItemFromStack(Object minecraftStack)
    {
        try
        {
            Class<?> cls = minecraftStack.getClass(); // net.minecraft.server.ItemStack
            Method methodGetItem = cls.getMethod("getItem");

            return methodGetItem.invoke(minecraftStack);
        }
        catch(IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException ex)
        {
            Bukkit.getLogger().log(Level.WARNING, "", ex);
            return null;
        }
    }

    public static String getMinecraftItemName(Object minecraftItem)
    {
        try
        {
            Class<?> cls = minecraftItem.getClass();
            Method methodGetName = cls.getMethod("getName");

            return (String) methodGetName.invoke(minecraftItem);
        }
        catch(IllegalAccessException | IllegalArgumentException | NoSuchMethodException | SecurityException | InvocationTargetException ex)
        {
            Bukkit.getLogger().log(Level.WARNING, "", ex);
            return "";
        }
    }

    public static String getNBTString(Object minecraftStack)
    {
        try
        {
            Class<?> cls = minecraftStack.getClass();
            Method methodSave = null;
            Class<?> nbtClass = null;
            for (Method method : cls.getMethods())
            {
                if (method.getName().equals("save") && method.getParameterTypes().length == 1)
                {
                    methodSave = method;
                    nbtClass = method.getParameterTypes()[0];
                    break;
                }
            }
            if (methodSave == null) {
                return "";
            }
            Object nbtCompound = nbtClass.newInstance();
            methodSave.invoke(minecraftStack, nbtCompound);

            return nbtCompound.toString();
        }
        catch(IllegalAccessException | IllegalArgumentException | InstantiationException | SecurityException | InvocationTargetException ex)
        {
            Bukkit.getLogger().log(Level.WARNING, "", ex);
            return "";
        }
    }

    public static String toNBTString(ItemStack stack)
    {
        Object nmsStack = getMinecraftStack(stack);
        if (nmsStack == null) {
            return "";
        }

        return getNBTString(nmsStack);
    }

    public static int removeFromInventory(PlayerInventory inventory, ItemStack items, boolean ignoreDurability)
    {
        HashMap<Integer, ItemStack> allRest = inventory.removeItem(items);
        int remainder = allRest.isEmpty() ? 0 : allRest.get(0).getAmount();

        if (remainder == 0) {
            return 0;
        }

        ItemStack[] armor = inventory.getArmorContents();
        for (int i = 0; i < armor.length; ++i)
        {
            ItemStack armorSlot = armor[i];
            if (compareStacks(armorSlot, items, ignoreDurability))
            {
                int diff = armorSlot.getAmount() - remainder;
                if (diff > 0)
                {
                    armorSlot.setAmount(diff);
                    remainder = 0;
                    break;
                }
                else
                {
                    armor[i] = null;
                    remainder = -diff;
                    if (remainder == 0) {
                        break;
                    }
                }
            }
        }

        inventory.setArmorContents(armor);
        if (remainder == 0) {
            return 0;
        }

        if (!ignoreDurability) {
            return remainder;
        }

        for (int slot = 0; slot < inventory.getSize(); ++slot)
        {
            ItemStack atSlot = inventory.getItem(slot);
            if (atSlot != null && compareStacks(atSlot, items, true))
            {
                int take = Math.min(remainder, atSlot.getAmount());
                ItemStack takeStack = new ItemStack(atSlot);
                takeStack.setAmount(take);

                inventory.removeItem(takeStack);
                remainder -= take;

                if (remainder < 1) {
                    return 0;
                }
            }
        }

        return remainder;
    }
}
