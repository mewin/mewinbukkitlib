/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.util;

import de.mewin.mewinbukkitlib.MewinBukkitLibPlugin;
import de.mewin.mewinbukkitlib.constants.MetaKeys;
import de.mewin.mewinbukkitlib.event.LocaleChangedEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

import java.util.List;

/**
 *
 * @author mewin
 */
public class PlayerUtil
{

    public static void setPlayerMeta(Plugin plugin, Player player, String key, Object value)
    {
        player.setMetadata(key, new FixedMetadataValue(plugin, value));
    }

    public static MetadataValue getPlayerMeta(Plugin plugin, Player player, String key)
    {
        List<MetadataValue> metadata = player.getMetadata(key);

        for (MetadataValue value : metadata) {
            if (value.getOwningPlugin() == plugin) {
                return value;
            }
        }

        return null;
    }

    public static void setLocale(Player player, String locale)
    {
        MewinBukkitLibPlugin plugin = MewinBukkitLibPlugin.get();
        MetadataValue currentLocale = getPlayerMeta(plugin, player, MetaKeys.LOCALE);
        if (currentLocale != null && currentLocale.asString().equals(locale)) {
            return;
        }

        setPlayerMeta(MewinBukkitLibPlugin.get(), player, MetaKeys.LOCALE, locale);

        Bukkit.getPluginManager().callEvent(new LocaleChangedEvent(player, locale));
    }
}
