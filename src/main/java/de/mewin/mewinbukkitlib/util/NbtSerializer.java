/*
 * Copyright (c) 2018 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mewin
 */
public class NbtSerializer
{
    public static Object deserializeNbtValue(Object value)
    {
        if (value instanceof Map) {
            return deserializeCompound((Map<String, Object>) value);
        } else if (value instanceof List) {
            return deserializeList((List) value);
        }
        String asString = String.valueOf(value);
        if (asString.isEmpty()) {
            throw new IllegalArgumentException("invalid nbt");
        }
        char chr = asString.charAt(0);
        switch (chr)
        {
            case 'b':
                return Byte.valueOf(asString.substring(1));
            case 's':
                return Short.valueOf(asString.substring(1));
            case 'i':
                return Integer.valueOf(asString.substring(1));
            case 'l':
                return Long.valueOf(asString.substring(1));
            case 'f':
                return Float.valueOf(asString.substring(1));
            case 'd':
                return Double.valueOf(asString.substring(1));
            case 'S':
                return asString.substring(1);
            default:
                throw new IllegalArgumentException("unknown nbt data type");
        }
    }

    public static NbtFactory.NbtCompound deserializeCompound(Map<String, Object> map)
    {
        NbtFactory.NbtCompound compound = NbtFactory.createCompound();
        for (Map.Entry<String, Object> entry : map.entrySet())
        {
            String key = entry.getKey();
            Object value = deserializeNbtValue(entry.getValue());

            compound.put(key, value);
        }
        return compound;
    }

    public static NbtFactory.NbtList deserializeList(List list)
    {
        NbtFactory.NbtList result = NbtFactory.createList();
        for (Object value : list) {
            result.add(deserializeNbtValue(value));
        }
        return result;
    }

    public static Object serializeNbtValue(Object value)
    {
        if (value instanceof NbtFactory.NbtCompound) {
            return serializeCompound((NbtFactory.NbtCompound) value);
        } else if (value instanceof NbtFactory.NbtList) {
            return serializeList((NbtFactory.NbtList) value);
        } else if (value instanceof Byte) {
            return "b" + ((Byte) value);
        } else if (value instanceof Short) {
            return "s" + ((Short) value);
        } else if (value instanceof Integer) {
            return "i" + ((Integer) value);
        } else if (value instanceof Long) {
            return "l" + ((Long) value);
        } else if (value instanceof Float) {
            return "f" + ((Float) value);
        } else if (value instanceof Double) {
            return "d" + ((Double) value);
        } else {
            return "S" + String.valueOf(value);
        }
    }

    public static Map<String, Object> serializeCompound(NbtFactory.NbtCompound compound)
    {
        HashMap<String, Object> result = new HashMap<>();
        for (Map.Entry<String, Object> entry : compound.entrySet())
        {
            String key = entry.getKey();
            Object value = serializeNbtValue(entry.getValue());

            result.put(key, value);
        }
        return result;
    }

    public static List serializeList(NbtFactory.NbtList list)
    {
        ArrayList result = new ArrayList();
        for (Object value : list) {
            result.add(serializeNbtValue(value));
        }
        return result;
    }
}
