/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.util;

/**
 *
 * @author mewin
 */
public class MathUtil
{
    public static <T extends Comparable<T>> T limit(T value, T min, T max)
    {
        if (value.compareTo(min) < 0) {
            return min;
        }

        if (value.compareTo(max) > 0) {
            return max;
        }

        return value;
    }
}
