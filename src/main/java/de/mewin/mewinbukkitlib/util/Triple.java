package de.mewin.mewinbukkitlib.util;

import java.util.Objects;

public class Triple<T, S, R>
{
    private final T first;
    private final S second;
    private final R third;

    public Triple(T first, S second, R third)
    {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public T getFirst()
    {
        return first;
    }

    public S getSecond()
    {
        return second;
    }

    public R getThird()
    {
        return third;
    }

    @Override
    public int hashCode()
    {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.first);
        hash = 61 * hash + Objects.hashCode(this.second);
        hash = 61 * hash + Objects.hashCode(this.third);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Triple<?, ?, ?> other = (Triple<?, ?, ?>) obj;
        if (!Objects.equals(this.first, other.first))
        {
            return false;
        }
        if (!Objects.equals(this.second, other.second))
        {
            return false;
        }
        if (!Objects.equals(this.third, other.third))
        {
            return false;
        }
        return true;
    }
}
