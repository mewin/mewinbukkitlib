package de.mewin.mewinbukkitlib.util;

import java.util.ArrayList;

public class TextUtil
{
    public static ArrayList<String> wrapText(String text, int length)
    {
        ArrayList<String> result = new ArrayList<>();

        while (text.length() > length)
        {
            int space = text.lastIndexOf(" ", length);
            if (space >= 0)
            {
                result.add(text.substring(0, space));
                text = text.substring(space + 1);
            }
            else
            {
                result.add(text.substring(0, length));
                text = text.substring(length);
            }
        }

        if (!"".equals(text))
        {
            result.add(text);
        }

        return result;
    }
}
