package de.mewin.mewinbukkitlib.util;

import org.bukkit.Bukkit;
import org.bukkit.World;

import java.util.UUID;

public class BukkitUtil
{
    public static World findWorld(String nameOrUuid)
    {
        try
        {
            UUID uuid = UUID.fromString(nameOrUuid);
            World world = Bukkit.getWorld(uuid);
            if (world != null) {
                return world;
            }
        }
        catch(IllegalArgumentException ignore) {}

        return Bukkit.getWorld(nameOrUuid);
    }

    public static boolean isWorld(World world, String nameOrUuid)
    {
        if (world.getName().equals(nameOrUuid)) {
            return true;
        }
        try {
            return world.getUID().equals(UUID.fromString(nameOrUuid));
        } catch(IllegalArgumentException ex) {
            return false;
        }
    }
}
