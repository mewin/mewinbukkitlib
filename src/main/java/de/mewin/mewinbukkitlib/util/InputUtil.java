/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.util;

/**
 *
 * @author mewin
 */
public class InputUtil
{
    public static int toInt(String str, int def)
    {
        try
        {
            return Integer.valueOf(str);
        }
        catch(NumberFormatException ex)
        {
            return def;
        }
    }

    public static int intDivCeil(int i1, int i2)
    {
        return (int) Math.ceil((double) i1 / (double) i2);
    }
}
