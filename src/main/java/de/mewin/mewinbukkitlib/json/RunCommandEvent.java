/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.json;

import org.json.simple.JSONObject;

/**
 *
 * @author mewin
 */
public class RunCommandEvent extends ClickEvent
{
    private final String command;

    public RunCommandEvent(String command)
    {
        this.command = command;
    }

    @Override
    public JSONObject toJSON()
    {
        JSONObject json = new JSONObject();
        json.put("action", "run_command");
        json.put("value", command);
        return json;
    }
}
