/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.json;

import de.mewin.mewinbukkitlib.util.Items;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONObject;

/**
 *
 * @author mewin
 */
public class ShowItemEvent extends HoverEvent
{
    private final ItemStack items;

    public ShowItemEvent(ItemStack items)
    {
        this.items = items.clone();
    }

    @Override
    public JSONObject toJSON()
    {
        JSONObject json = new JSONObject();
        json.put("action", "show_item");
        json.put("value", Items.toNBTString(items));
        return json;
    }
}
