/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.json;

import org.json.simple.JSONObject;

/**
 *
 * @author mewin
 */
public class ShowTextEvent extends HoverEvent
{
    private final String text;

    public ShowTextEvent(String text)
    {
        this.text = text;
    }

    @Override
    public JSONObject toJSON()
    {
        JSONObject json = new JSONObject();
        json.put("action", "show_text");
        json.put("value", text.trim()); // texts from file may contain whitespaces
        return json;
    }

}
