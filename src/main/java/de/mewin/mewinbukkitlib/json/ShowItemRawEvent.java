/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.json;

import org.json.simple.JSONObject;

/**
 *
 * @author mewin
 */
public class ShowItemRawEvent extends HoverEvent
{
    private final String itemRaw;

    public ShowItemRawEvent(String itemRaw)
    {
        this.itemRaw = itemRaw;
    }

    @Override
    public JSONObject toJSON()
    {
        JSONObject json = new JSONObject();
        json.put("action", "show_item");
        json.put("value", itemRaw);
        return json;
    }
}
