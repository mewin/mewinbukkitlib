/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.json;

import org.json.simple.JSONObject;

/**
 *
 * @author mewin
 */
public abstract class Event
{
    public abstract JSONObject toJSON();
}
