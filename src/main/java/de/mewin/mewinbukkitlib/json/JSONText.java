/*
 * Copyright (c) 2017 mewin
 * All rights reserved
 */
package de.mewin.mewinbukkitlib.json;

import java.util.ArrayList;
import org.json.simple.JSONObject;

/**
 *
 * @author mewin
 */
public class JSONText
{
    private final String text;
    private final String insertion;
    private final HoverEvent hoverEvent;
    private final ClickEvent clickEvent;

    public JSONText(String text, String insertion, HoverEvent hoverEvent, ClickEvent clickEvent)
    {
        this.text = text;
        this.insertion = insertion;
        this.hoverEvent = hoverEvent;
        this.clickEvent = clickEvent;
    }

    public String getText()
    {
        return text;
    }

    public JSONObject toJSON()
    {
        JSONObject json = new JSONObject();
        json.put("text", text);
        if (insertion != null) {
            json.put("insertion", insertion);
        }
        if (hoverEvent != null) {
            json.put("hoverEvent", hoverEvent.toJSON());
        }
        if (clickEvent != null) {
            json.put("clickEvent", clickEvent.toJSON());
        }
        return json;
    }

    public static class Builder
    {
        private String text;
        private String insertion;
        private HoverEvent hoverEvent;
        private ClickEvent clickEvent;

        public Builder()
        {
            text = null;
        }

        public Builder setText(String text)
        {
            this.text = text;
            return this;
        }

        public Builder setInsertion(String insertion)
        {
            this.insertion = insertion;
            return this;
        }

        public Builder setHoverEvent(HoverEvent hoverEvent)
        {
            this.hoverEvent = hoverEvent;
            return this;
        }

        public Builder setClickEvent(ClickEvent clickEvent)
        {
            this.clickEvent = clickEvent;
            return this;
        }

        public boolean isReady()
        {
            return this.text != null;
        }

        public JSONText build()
        {
            return new JSONText(text, insertion, hoverEvent, clickEvent);
        }
    }

    public static class MultiBuilder
    {
        private final ArrayList<JSONText> texts;
        private Builder builder;

        public MultiBuilder()
        {
            super();

            builder = new Builder();
            texts = new ArrayList<>();

            texts.add(new JSONText.Builder().setText("").build());
        }

        public MultiBuilder setText(String text)
        {
            builder.setText(text);
            return this;
        }

        public MultiBuilder setInsertion(String insertion)
        {
            builder.setInsertion(insertion);
            return this;
        }

        public MultiBuilder setHoverEvent(HoverEvent hoverEvent)
        {
            builder.setHoverEvent(hoverEvent);
            return this;
        }

        public MultiBuilder setClickEvent(ClickEvent clickEvent)
        {
            builder.setClickEvent(clickEvent);
            return this;
        }

        public MultiBuilder next()
        {
            if (builder.isReady()) {
                texts.add(builder.build());
            }
            builder = new Builder();
            return this;
        }

        public JSONText[] build()
        {
            if (builder.isReady()) {
                next();
            }
            return texts.toArray(new JSONText[texts.size()]);
        }
    }
}
